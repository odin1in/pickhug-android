package com.shenlearn.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import API.pickhug.ArticleService;
import API.pickhug.UserService;
import Adapter.ForumCardViewDataAdapter;
import Model.Article;
import Model.Comment;

public class MyArticleActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Article> articleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);


        mRecyclerView = (RecyclerView) findViewById(R.id.forum_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        this.fetchData();




    }

    public void fetchData(){
        final UserService articleservice = new UserService();
        progressBar.setVisibility(View.VISIBLE);
        articleservice.articles(this, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                articleservice.error_handler(MyArticleActivity.this, 999, "");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                Looper.loop();
            }
            //
            @Override
            public void onResponse(final Response response) throws IOException {
                Integer statusCode = response.code();

                Looper.prepare();
                switch (statusCode) {
                    case 200:
                        try{
                            articleList = new ArrayList<>();
                            JSONArray array = new JSONArray(response.body().string());

                            for(int i = 0; i < array.length(); i++){
                                JSONObject articleJSON = array.getJSONObject(i);
                                Log.v("1", articleJSON.getString("created_at"));
                                Article article = new Article();
                                article.setRawData(articleJSON.toString());
                                article.setId(articleJSON.getInt("id"));
                                article.setContent(articleJSON.getString("content"));
                                article.setCreatedAt(articleJSON.getString("created_at"));
                                article.setKnowsCount(articleJSON.getInt("knows_count"));
                                JSONArray commentsJSON = articleJSON.getJSONArray("comments");
                                for(int j = 0; j < commentsJSON.length(); j++){
                                    JSONObject commentJSON = commentsJSON.getJSONObject(j);
                                    Comment comment = new Comment();
                                    comment.setContent(commentJSON.getString("content"));
                                    Log.v("1", commentJSON.toString());
                                }
                                articleList.add(article);
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter = new ForumCardViewDataAdapter(MyArticleActivity.this, articleList);
                                    mRecyclerView.setAdapter(mAdapter);
                                }
                            });
                        }catch (Exception error){
                            articleservice.error_handler(MyArticleActivity.this, 999, "");
                            error.printStackTrace();
                        }

                        break;
                    default:
                        articleservice.error_handler(MyArticleActivity.this, statusCode, "");
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                Looper.loop();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.forum_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.new_article) {

            Intent intent = new Intent(this, NewArticleActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(intent, 90);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 90:
                if (resultCode == 201) {
                    Toast.makeText(this, "成功發佈貼文。", Toast.LENGTH_SHORT).show();
                    this.fetchData();
                }
                break;
        }
    }

}
