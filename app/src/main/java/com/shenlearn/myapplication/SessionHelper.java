package com.shenlearn.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import Model.Preferences;

/**
 * Created by odin1in on 15/10/15.
 */
public class SessionHelper {
    Context _context;

    public SessionHelper(Context context){
        this._context = context;
    }

    public void check_login(){
        String accesstoken = new Preferences().getAccessToken(_context);
        if(accesstoken.isEmpty()){
            redirect_to_login();
        }
    }

    public void redirect_to_login(){
        Intent intent = new Intent(_context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
//        Toast.makeText(_context, "Token 失效，請重新登入。", Toast.LENGTH_SHORT).show();
    }

    public void logout(){
        new Preferences().clear(_context);
        Intent intent = new Intent(_context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }

}
