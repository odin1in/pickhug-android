package com.shenlearn.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import API.pickhug.CommentService;

public class ReplyActivity extends AppCompatActivity {
    public Integer article_id;
    public ListView reply_listview;
    public String article_raw_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        article_id = intent.getIntExtra("article_id", 0);
        article_raw_data = intent.getStringExtra("article_raw_data");
        Log.v("1", article_id.toString());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//
        Button reply = (Button) findViewById(R.id.forum_card_view_reply);
        Button know_you = (Button) findViewById(R.id.forum_card_view_know);
        TextView known = (TextView) findViewById(R.id.forum_card_view_known);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) known.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        reply.setVisibility(View.GONE);
        know_you.setVisibility(View.GONE);


        this.fetchData(article_raw_data);
    }

    public void fetchData(String raw_data){
        CommentService commentService = new CommentService();

        try{
            JSONObject articleJSON = new JSONObject(raw_data);
            JSONArray commentsJSON = new JSONArray(articleJSON.getString("comments"));
            TextView content = (TextView) findViewById(R.id.forum_card_view_content);
            content.setText(articleJSON.getString("content"));
            TextView known = (TextView) findViewById(R.id.forum_card_view_known);
            known.setText(articleJSON.getString("knows_count") + " 人懂你");
            TextView datetime = (TextView) findViewById(R.id.forum_card_view_datetime);
            datetime.setText(articleJSON.getString("created_at"));
            final ArrayList<HashMap> list = new ArrayList<>();
            for(int i=0; i<commentsJSON.length(); i++){
                JSONObject commentJSON = commentsJSON.getJSONObject(i);
                JSONObject userJSON = commentJSON.getJSONObject("user");
                HashMap <String,String> item = new HashMap<String,String>();
                item.put("content", commentJSON.getString("content"));
                item.put("user_name", userJSON.getString("name"));
                list.add( item );

            }
            reply_listview = (ListView) findViewById(R.id.reply_listview);

            ArrayAdapter<HashMap> adapter = new ArrayAdapter<HashMap>(this, android.R.layout.simple_list_item_2, android.R.id.text1, list) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                    TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                    HashMap <String,String> item1 = list.get(position);

                    text1.setText(item1.get("user_name"));
                    text2.setText(item1.get("content"));
                    return view;
                }
            };

            reply_listview.setAdapter(adapter);

        }catch (Exception error){
            commentService.error_handler(this, 998, "");
        }
        Log.v("1", raw_data);


    }

    public void send_reply(View view){
        final EditText reply_content = (EditText) findViewById(R.id.reply_text);
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", "請稍候 ...", false, false);
        final CommentService commentService = new CommentService();
        final Context mContext = this;
        HashMap params = new HashMap();
        params.put("comment[content]", reply_content.getText());
        commentService.create(this, article_id, params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                commentService.error_handler(mContext, 999, "");
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) throws IOException {

                Looper.prepare();
                RegisterActivity.locked = false;
                Integer statusCode = response.code();

                switch (statusCode) {
                    case 201:
                        try{
                            final JSONObject articleJSON = new JSONObject(response.body().string());
                            ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                    ReplyActivity activity = (ReplyActivity) mContext;
                                    activity.fetchData(articleJSON.toString());
                                    reply_content.setText("");
                                }
                            });
                        }catch (Exception error){
                            commentService.error_handler(mContext, 999, "");
                            error.printStackTrace();
                        }

                        break;
                    default:
                        commentService.error_handler(mContext, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
    }

}
