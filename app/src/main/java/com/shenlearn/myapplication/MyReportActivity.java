package com.shenlearn.myapplication;

import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import API.pickhug.ReportService;
import API.pickhug.UserService;
import Adapter.MyReportCardViewDataAdapter;
import Model.Article;
import Model.Report;

public class MyReportActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Report> reportList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        this.fetchData();
    }

    public void fetchData(){
        final UserService reportservice = new UserService();
        progressBar.setVisibility(View.VISIBLE);
        reportservice.reports(this, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                reportservice.error_handler(MyReportActivity.this, 999, "");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                Looper.loop();
            }

            //
            @Override
            public void onResponse(final Response response) throws IOException {
                Integer statusCode = response.code();

                Looper.prepare();
                switch (statusCode) {
                    case 200:
                        try {
                            reportList = new ArrayList<>();
                            JSONArray array = new JSONArray(response.body().string());
                            Log.v("1", array.toString());
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject reportJSON = array.getJSONObject(i);
                                Report report = new Report();
                                report.setId(reportJSON.getInt("id"));
                                report.setNote(reportJSON.getString("note"));
                                report.setViolateAt(reportJSON.getString("violate_at"));
                                report.setLocation(reportJSON.getString("location"));
                                report.setStatus(reportJSON.getString("status"));
                                report.setPic1(reportJSON.getString("pic1"));
                                report.setPic2(reportJSON.getString("pic2"));
                                report.setPic3(reportJSON.getString("pic3"));

                                reportList.add(report);
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter = new MyReportCardViewDataAdapter(MyReportActivity.this, reportList);
                                    mRecyclerView.setAdapter(mAdapter);
                                }
                            });
                        } catch (Exception error) {
                            reportservice.error_handler(MyReportActivity.this, 999, "");
                            error.printStackTrace();
                        }

                        break;
                    default:
                        reportservice.error_handler(MyReportActivity.this, statusCode, "");
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                Looper.loop();
            }
        });

    }

}
