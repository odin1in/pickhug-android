package com.shenlearn.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import API.pickhug.UserService;
import Adapter.ForumCardViewDataAdapter;
import Model.Article;
import Model.Comment;

public class ProfileActivity extends AppCompatActivity {
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.fetchData();
    }

    public void fetchData(){

        final UserService userService = new UserService();
        progressBar.setVisibility(View.VISIBLE);

        userService.show(this, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                userService.error_handler(ProfileActivity.this, 999, "");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Integer statusCode = response.code();

                Looper.prepare();
                switch (statusCode) {
                    case 200:
                        try {
                            final JSONObject userJSON = new JSONObject(response.body().string());
                            final String email = userJSON.getString("email");
                            final String name = userJSON.getString("name");
                            final String address = userJSON.getString("address");
                            final String phone = userJSON.getString("phone");
                            final String identity = userJSON.getString("identity");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    EditText email_text = (EditText) findViewById(R.id.email);
                                    email_text.setText(email);
                                    EditText password_text = (EditText) findViewById(R.id.password);
                                    EditText name_text = (EditText) findViewById(R.id.name);
                                    name_text.setText(name);
                                    EditText address_text = (EditText) findViewById(R.id.address);
                                    address_text.setText(address);
                                    EditText phone_text = (EditText) findViewById(R.id.phone);
                                    phone_text.setText(phone);
                                    EditText identity_text = (EditText) findViewById(R.id.identity);
                                    identity_text.setText(identity);
                                }
                            });
                        } catch (Exception error) {
                            userService.error_handler(ProfileActivity.this, 999, "");
                            error.printStackTrace();
                        }

                        break;
                    default:
                        userService.error_handler(ProfileActivity.this, statusCode, "");
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                Looper.loop();
            }
        });



    }

    public void updateUser(View view){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(ProfileActivity.this, "", "請稍候 ...", false, false);

        HashMap params = new HashMap();
        EditText email = (EditText) findViewById(R.id.email);
        params.put("user[email]", email.getText());

        EditText phone = (EditText) findViewById(R.id.phone);
        params.put("user[phone]", phone.getText());

        EditText password = (EditText) findViewById(R.id.password);
        params.put("user[password]", password.getText());

        EditText name = (EditText) findViewById(R.id.name);
        params.put("user[name]", name.getText());

        EditText address = (EditText) findViewById(R.id.address);
        params.put("user[address]", address.getText());

        EditText identity = (EditText) findViewById(R.id.identity);
        params.put("user[identity]", identity.getText());

        final UserService userservice = new UserService();
        userservice.update(this, params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                userservice.error_handler(ProfileActivity.this, 999, "");
                Looper.loop();

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Looper.prepare();
                RegisterActivity.locked = false;
                Integer statusCode = response.code();
                switch (statusCode) {
                    case 200:
                        ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Toast.makeText(ProfileActivity.this, "成功更新。", Toast.LENGTH_SHORT).show();
                            }
                        });

                        break;
                    default:
                        userservice.error_handler(ProfileActivity.this, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
    }

}
