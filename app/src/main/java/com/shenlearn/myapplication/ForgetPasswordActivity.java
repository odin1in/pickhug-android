package com.shenlearn.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;

import API.pickhug.UserService;

public class ForgetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void forget_password(View view) {
        Log.v("1", "forget_password");
        final UserService userservice = new UserService();
        final ProgressDialog ringProgressDialog = ProgressDialog.show(ForgetPasswordActivity.this, "", "請稍候 ...", false, false);

        HashMap params = new HashMap();
        EditText email = (EditText) findViewById(R.id.email);
        params.put("email", email.getText());

        userservice.forgot(params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                userservice.error_handler(ForgetPasswordActivity.this, 999, "");
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Looper.prepare();
                RegisterActivity.locked = false;
                Integer statusCode = response.code();
                switch (statusCode) {
                    case 200:
                        ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Intent intent = new Intent();
                                setResult(200, intent);
                                finish();
                            }
                        });

                        break;
                    default:
                        userservice.error_handler(ForgetPasswordActivity.this, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
    }

}
