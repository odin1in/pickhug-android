package com.shenlearn.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.util.HashMap;

import API.pickhug.UserService;

public class RegisterActivity extends AppCompatActivity {
    static boolean locked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void createUser(View view) {

        final ProgressDialog ringProgressDialog = ProgressDialog.show(RegisterActivity.this, "", "請稍候 ...", false, false);

        HashMap params = new HashMap();
        EditText email = (EditText) findViewById(R.id.email);
        params.put("user[email]", email.getText());

        EditText phone = (EditText) findViewById(R.id.phone);
        params.put("user[phone]", phone.getText());

        EditText password = (EditText) findViewById(R.id.password);
        params.put("user[password]", password.getText());

        EditText name = (EditText) findViewById(R.id.name);
        params.put("user[name]", name.getText());

        EditText address = (EditText) findViewById(R.id.address);
        params.put("user[address]", address.getText());

        EditText identity = (EditText) findViewById(R.id.identity);
        params.put("user[identity]", identity.getText());

        final UserService userservice = new UserService();
        userservice.create(params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                userservice.error_handler(RegisterActivity.this, 999, "");
                Looper.loop();

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Looper.prepare();
                RegisterActivity.locked = false;
                Integer statusCode = response.code();
                switch (statusCode) {
                    case 201:
                        ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Intent intent = new Intent();
                                setResult(201, intent);
                                finish();
                            }
                        });

                        break;
                    default:
                        userservice.error_handler(RegisterActivity.this, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
    }
}