package com.shenlearn.myapplication;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import API.pickhug.AccessTokenService;
import Model.Preferences;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Preferences p = new Preferences();
        setTitle("登入");
    }

    public void login(View view){
        final AccessTokenService accesstokenService = new AccessTokenService();
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", "請稍候 ...", false, false);

        HashMap params = new HashMap();
        EditText email = (EditText) findViewById(R.id.email);
        params.put("email", email.getText());

        EditText password = (EditText) findViewById(R.id.password);
        params.put("password", password.getText());

        accesstokenService.create(params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                accesstokenService.error_handler(MainActivity.this, 999, "");
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Looper.prepare();
                Integer statusCode = response.code();
                switch (statusCode) {
                    case 201:
                        try{
                            JSONObject obj = new JSONObject(response.body().string());
                            Preferences pref = new Preferences();
                            pref.setAccessToken(MainActivity.this, obj.get("uuid").toString());
                            pref.setUserId(MainActivity.this, obj.get("user_id").toString());
                            Log.v("1", obj.toString());
                        }catch (Exception error){
                            accesstokenService.error_handler(MainActivity.this, 999, "");
                            error.printStackTrace();
                        }


                        ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                                startActivity(intent);
                            }
                        });

                        break;
                    default:
                        accesstokenService.error_handler(MainActivity.this, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        boolean login = true;
//
//        if(login){
//            Intent intent = new Intent(this, DashboardActivity.class);
//            startActivity(intent);
//
//        }else{
//            builder.setMessage("帳號或密碼無效。");
//            builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                }
//            });
//
//            AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//        }
    }

    public void forget_password(View view){
        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        startActivityForResult(intent, 91);
    }

    public void go_register(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, 90);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 90:
                if (resultCode == 201) {
                    Toast.makeText(this, "成功註冊。", Toast.LENGTH_SHORT).show();
                }
                break;
            case 91:
                if (resultCode == 200) {
                    Toast.makeText(this, "重設密碼已寄至您信箱。", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
