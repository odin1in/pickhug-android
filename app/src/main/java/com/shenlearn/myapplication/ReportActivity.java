package com.shenlearn.myapplication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import API.pickhug.ArticleService;
import API.pickhug.ReportService;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class ReportActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private EditText address;
    private List<ImageView> imageList;
    private List<String>    images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        address = (EditText) findViewById(R.id.report_location);

        EditText report_date = (EditText) findViewById(R.id.report_date);
        EditText report_time = (EditText) findViewById(R.id.report_time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String currentDateandTime = sdf.format(new Date());
        report_date.setText(currentDateandTime);
        SimpleDateFormat abc = new SimpleDateFormat("HHmm");
        String currentDateandTime2 = abc.format(new Date());
        report_time.setText(currentDateandTime2);

        imageList = new ArrayList<>();
        images = new ArrayList<>();
        ImageView image10 = (ImageView) findViewById(R.id.imageView10);
        ImageView image11 = (ImageView) findViewById(R.id.imageView11);
        ImageView image12 = (ImageView) findViewById(R.id.imageView12);

        imageList.add(image10);
        imageList.add(image11);
        imageList.add(image12);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.article_spinners, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(isNetworkEnabled){
            Log.v("1", "network enable");
        }
        if(isGPSEnabled){
            Log.v("1", "GPS enable");
        }

        if(!isGPSEnabled && !isNetworkEnabled)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReportActivity.this);

            // Setting Dialog Title
            alertDialog.setTitle("GPS settings");

            // Setting Dialog Message
            alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

            // On pressing Settings button
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            // on pressing cancel button
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();

        }else{
            if (locationManager != null) {


                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                    Toast.makeText(this, "This app relies on location data for it's main functionality. Please enable GPS data to access all features.", Toast.LENGTH_LONG).show();
                } else {

                    int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
                    if (result == PackageManager.PERMISSION_GRANTED){
                        Log.v("3", "no1");
                        LocationListener locLis = new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                                String result = getAddressByLocation(location);
//                                Toast.makeText(getApplication(), "latitude: " + location.getLatitude() + " longitude: " + location.getLongitude(), Toast.LENGTH_LONG).show();

                                if(result.matches("Sorry! Geocoder service not Present.")){
                                    Toast.makeText(getApplication(), getAddressByLocation(location), Toast.LENGTH_LONG).show();
                                }else{
                                    address.setText(result);
                                }
                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                                Log.v("2", "change");
                            }

                            @Override
                            public void onProviderEnabled(String provider) {
                                Log.v("3", "change");
                            }

                            @Override
                            public void onProviderDisabled(String provider) {
                                Log.v("4", "change");
                            }
                        };

                        if(isGPSEnabled){
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 1, locLis);
                            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                double latitude = location.getLatitude();
                                double longitude = location.getLongitude();

                                Log.v("3", "no");
                            }
                        }
                        if(isNetworkEnabled){
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5, 1, locLis);
                            Location location2 = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location2 != null) {
                                double latitude = location2.getLatitude();
                                double longitude = location2.getLongitude();
//                                Toast.makeText(getApplication(), "latitude: " + latitude + " longitude: " + longitude, Toast.LENGTH_LONG).show();
                                Log.v("4", "no");
                            }
                        }





                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }
                }
//                Log.v("1","test");

            }
        }
    }

    public void create_report(View view){
        final ReportService reportservice = new ReportService();
        final ProgressDialog ringProgressDialog = ProgressDialog.show(ReportActivity.this, "", "請稍候 ...", false, false);

        HashMap files = new HashMap();
        HashMap params = new HashMap();
        EditText note = (EditText) findViewById(R.id.report_note);
        params.put("report[note]", note.getText());
        EditText date = (EditText) findViewById(R.id.report_date);
        EditText time = (EditText) findViewById(R.id.report_time);
        EditText location = (EditText) findViewById(R.id.report_location);
        EditText others = (EditText) findViewById(R.id.report_others);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        params.put("report[location]", location.getText());
        params.put("report[category]", spinner.getSelectedItem().toString());
        params.put("report[category_other]", others.getText());

        for(int i = 0; i < images.size(); i++){
            files.put("report[pic" + (i+1) + "]", images.get(i));
        }

        try{

            DateFormat df = new SimpleDateFormat("yyyyMMdd");
            Date result =  df.parse(date.getText().toString());

            DateFormat df2 = new SimpleDateFormat("HHmm");
            Date result2 =  df2.parse(time.getText().toString());


        }catch (Exception error){
            Toast.makeText(this, "日期格式不合法。", Toast.LENGTH_SHORT).show();
            ringProgressDialog.dismiss();
            error.printStackTrace();
            return;
        }
        params.put("report[violate_at]", date.getText() + " " + time.getText());

        reportservice.create(ReportActivity.this, params, files, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                reportservice.error_handler(ReportActivity.this, 999, "");
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Looper.prepare();
                RegisterActivity.locked = false;
                Integer statusCode = response.code();
                switch (statusCode) {
                    case 201:
                        ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Intent intent = new Intent();
                                setResult(201, intent);
                                finish();
                            }
                        });

                        break;
                    default:
                        reportservice.error_handler(ReportActivity.this, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
    }

    public String getAddressByLocation(Location location) {
        String returnAddress = "";
        try {
            if (location != null) {
                Double longitude = location.getLongitude();	//取得經度
                Double latitude = location.getLatitude();	//取得緯度

                //建立Geocoder物件: Android 8 以上模疑器測式會失敗
                Geocoder gc = new Geocoder(this, Locale.TRADITIONAL_CHINESE); 	//地區:台灣
                //自經緯度取得地址
                List<Address> lstAddress = gc.getFromLocation(latitude, longitude, 1);
//                List<Address> lstAddress = lstAddress = gc.getFromLocationName("地址", 3);	//輸入地址回傳Location物件

                	if (!Geocoder.isPresent()){ //Since: API Level 9
                		returnAddress = "Sorry! Geocoder service not Present.";
                	}
                returnAddress = lstAddress.get(0).getAddressLine(0);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return returnAddress;
    }

    public void pickimage(View view){
        Intent intent = new Intent(this, MultiImageSelectorActivity.class);
        // whether show camera
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);

// max select image amount
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 3);

// select mode (MultiImageSelectorActivity.MODE_SINGLE OR MultiImageSelectorActivity.MODE_MULTI)
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);

        startActivityForResult(intent, 222);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("1", "ggyy");
        if(requestCode == 222){
            if(resultCode == RESULT_OK){
                // Get the result list of select image paths
                List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                for(int i = 0; i < imageList.size(); i ++){
                    imageList.get(i).setImageBitmap(null);
                }
                images = new ArrayList<>();
                for(int i = 0; i < path.size(); i ++){

                    Log.v("1", path.get(i));
                    File imgFile = new File(path.get(i));

                    if(imgFile.exists()){

                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        images.add(imgFile.getAbsolutePath());
                        imageList.get(i).setImageBitmap(myBitmap);
                        Log.v("2", images.get(i));
                    }
                }
                // do your logic ....
            }
        }
    }

}
