package com.shenlearn.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class CallActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private Location currentLocation;
    private boolean selected = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner spinner = (Spinner) findViewById(R.id.call_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.call_spinners, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(selected == true){
                    fetchLocation(parent.getItemAtPosition(position).toString(), null);
                }
                selected = true;
//
                Log.v("1", "selected");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                fetchLocation("中壢區", null);
            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(isNetworkEnabled){
            Log.v("1", "network enable");
        }
        if(isGPSEnabled){
            Log.v("1", "GPS enable");
        }

        if(!isGPSEnabled && !isNetworkEnabled)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(CallActivity.this);

            // Setting Dialog Title
            alertDialog.setTitle("GPS settings");

            // Setting Dialog Message
            alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

            // On pressing Settings button
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            // on pressing cancel button
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();

        }else{
            if (locationManager != null) {


                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                    Toast.makeText(this, "This app relies on location data for it's main functionality. Please enable GPS data to access all features.", Toast.LENGTH_LONG).show();
                } else {

                    int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
                    if (result == PackageManager.PERMISSION_GRANTED){
                        Log.v("3", "no1");
                        LocationListener locLis = new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
//                                Toast.makeText(getApplication(), "latitude: " + location.getLatitude() + " longitude: " + location.getLongitude(), Toast.LENGTH_LONG).show();
                                currentLocation = location;
                                fetchLocation("", location);

                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                                Log.v("2", "change");
                            }

                            @Override
                            public void onProviderEnabled(String provider) {
                                Log.v("3", "change");
                            }

                            @Override
                            public void onProviderDisabled(String provider) {
                                Log.v("4", "change");
                            }
                        };

                        if(isGPSEnabled){
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5, 1, locLis);
                            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                double latitude = location.getLatitude();
                                double longitude = location.getLongitude();

                                Log.v("3", "no");
                            }
                        }
                        if(isNetworkEnabled){
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5, 1, locLis);
                            Location location2 = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location2 != null) {
                                double latitude = location2.getLatitude();
                                double longitude = location2.getLongitude();
//                                Toast.makeText(getApplication(), "latitude: " + latitude + " longitude: " + longitude, Toast.LENGTH_LONG).show();
                                Log.v("4", "no");
                            }
                        }





                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }
                }
//                Log.v("1","test");

            }
        }

//
//        fetchLocation("中壢區", null);

    }

    public void fetchLocation(String country, Location location){
        ListView listview = (ListView) findViewById(R.id.call_listview);
        final Context mContext = this;
        final ArrayList<HashMap> list = new ArrayList<>();
        String[] name = {"警察局","桃園分局","桃園分局大樹派出所","桃園分局武陵派出所","桃園分局埔子派出所","桃園分局景福派出所","桃園分局中路派出所","桃園分局龍安派出所","桃園分局青溪派出所","桃園分局同安派出所","桃園交通分隊","中壢分局","中壢分局大崙派出所","中壢分局中福派出所","中壢分局中壢派出所","中壢分局仁愛派出所","中壢分局內壢派出所","中壢分局文化派出所","中壢分局普仁派出所","中壢分局興國派出所","中壢分局龍興派出所","中壢分局自強派出所","中壢分局青埔派出所","中壢交通分隊","大園分局","大園分局大園派出所","大園分局觀音分駐所","大園分局竹圍派出所","大園分局埔心派出所","大園分局草漯派出所","大園分局新坡派出所","大園分局潮音派出所","大園分局三果派出所","大園交通分隊","楊梅分局","楊梅分局幼獅派出所","楊梅分局新屋分駐所","楊梅分局楊梅派出所","楊梅分局永安派出所","楊梅分局大坡派出所","楊梅分局埔頂派出所","楊梅分局草湳派出所","楊梅分局上湖派出所","楊梅分局富岡派出所","楊梅交通小隊","平鎮分局","平鎮分局平鎮派出所","平鎮分局宋屋派出所","平鎮分局北勢派出所","平鎮分局建安派出所","平鎮分局龍岡派出所","平鎮交通分隊","龜山分局","龜山分局大坑派出所","龜山分局大林派出所","龜山分局大埔派出所","龜山分局坪頂派出所","龜山分局迴龍派出所","龜山分局龜山派出所","八德分局","八德分局四維派出所","八德分局八德派出所","八德分局大安派出所","八德分局廣興派出所","八德分局高明派出所","八德交通分隊","蘆竹分局","蘆竹分局南崁派出所","蘆竹分局大竹派出所","蘆竹分局南竹派出所","蘆竹分局外社派出所","蘆竹交通小隊","龍潭分局","龍潭分局龍潭派出所","龍潭分局中興派出所","龍潭分局聖亭派出所","龍潭分局石門派出所","龍潭分局高平派出所","龍潭分局三和派出所","龍潭交通分隊","大溪分局","大溪分局南雅派出所","大溪分局圳頂派出所","大溪分局三元派出所","大溪分局中新派出所","大溪分局永福派出所","大溪分局內柵派出所","大溪分局三層派出所","大溪分局福安派出所","大溪分局百吉派出所","大溪分局三民派出所","大溪分局復興分駐所","大溪分局霞雲派出所","大溪分局溪內派出所","大溪分局羅浮派出所","大溪分局奎輝派出所","大溪分局長興派出所","大溪分局榮華派出所","大溪分局三光派出所","大溪分局光華派出所","大溪分局巴陵派出所","大溪分局慈湖分駐所","大溪交通小隊"};
        String[] address = {"桃園市桃園區縣府路3號","桃園市桃園區文中北路2號","桃園市桃園區大林里樹仁二街41號","桃園市桃園區復興路135號","桃園市桃園區文中北路2號","桃園市桃園區民族路110號","桃園市桃園區縣府路51號1樓","桃園市桃園區龍安街140號","桃園市桃園區青溪里鎮撫街39號","桃園市桃園區大興西路1段153號","桃園市桃園區文中北路2號","桃園市中壢區延平路607號","桃園市中壢區中正路4段186號","桃園市中壢區南園二路86號","桃園市中壢區延平路607號","桃園市中壢區新中北路80巷75號","桃園市中壢區中華路1段324號","桃園市中壢區吉林路60號","桃園市中壢區日新路20號","桃園市中壢區中豐路88號","桃園市中壢區龍勇路139號","桃園市中壢區榮民路387號","桃園市中壢區青埔路一段67號","桃園市中壢區中和路210號","桃園市大園區中正西路13號","桃園市大園區中正西路13號","桃園市觀音區觀音里中山路14號","桃園市大園區竹圍里2鄰竹圍街10號","桃園市大園區埔心里14鄰1號","桃園市觀音區草漯里7鄰大觀路二段77號","桃園市觀音區大同里5鄰中山路2段668號","桃園市大園區北港里12鄰9號","桃園市大園區三石里15鄰三和路25號","桃園市大園區沙崙里1鄰港口52-1號","桃園市楊梅區中山路123號","桃園市楊梅區幼獅路一段458號","桃園市新屋區中山路239號","桃園市楊梅區中山路123號","桃園市新屋區中山西路三段62號","桃園市新屋區大坡里五鄰三角堀94號","桃園市新屋區埔頂里16鄰中華南路二段445號","桃園市新屋區埔心里永美路237號","桃園市新屋區楊湖路三段239號","桃園市新屋區富岡里富源街22號","桃園市中壢區民族路六段30號","桃園市平鎮區中庸路123號","桃園市平鎮區中庸路123號","桃園市平鎮區延平路二段412號","桃園市平鎮區振平街228號","桃園市平鎮區金陵路五段108號","桃園市平鎮區龍南路141號","桃園市平鎮區振平街228號","桃園市龜山區自強南路123號","桃園市龜山區大坑路一段851號","桃園市龜山區中興路158號","桃園市龜山區文德二路80號","桃園市龜山區大湖一路65號","桃園市龜山區萬壽路一段117號","桃園市龜山區萬壽路二段931號","桃園市八德區興豐路1216號","桃園市八德區重慶街148號","桃園市八德區中山路23號","桃園市八德區和平路頂僑街25號","桃園市八德區廣興路829號","桃園市八德區永豐路572巷17號","桃園市八德區興豐路1216號","桃園市蘆竹區南崁路175巷10號2樓","桃園市蘆竹區南崁村南崁路179號","桃園市蘆竹區大竹村大竹路540號","桃園市蘆竹區南竹路二段97號","桃園市蘆竹區外社村山林路二段405號","桃園市蘆竹區南崁路177-1號","桃園市龍潭區淩雲里干城路21號","桃園市龍潭區龍潭里東龍路198號","桃園市龍潭區中興里武漢路45號","桃園市龍潭區淩雲里干城路21號1樓","桃園市龍潭區佳安里文化路230號","桃園市龍潭區高平里中豐路高平段二八一號","桃園市龍潭區三和里直坑二四號","桃園市龍潭區淩雲里干城路21號1樓","桃園市大溪區普濟路九號","桃園市大溪區康莊路181號","桃園市大溪區公園路46號","桃園市大溪區石園路53號","桃園市大溪區大鶯路1481號","桃園市大溪區信義路1127號","桃園市大溪區康安里內柵路一段56號","桃園市大溪區福安里復興路一段385號","桃園市大溪區福安里復興路一段890號","桃園市大溪區復興里復興路二段209號","桃園市復興區三民里11鄰43號","桃園市復興區澤仁里中正路2號","桃園市復興區霞雲里6鄰21號","桃園市復興區羅浮里所2鄰合流段51-2號","桃園市復興區羅浮里三鄰羅浮3號","桃園市復興區輝里4鄰37號","桃園市復興區長興里16鄰頭角29號","桃園市復興區高義里11鄰2號","桃園市復興區三光里武道能敢7鄰33號","桃園市復興區華陵里4鄰12號","桃園市復興區華陵里9鄰44號","桃園市大溪區復興路一段1268號","桃園市大溪區公園路46號"};
        String[] lat = {"24.993062,121.301904","25.000169,121.29907","24.978062,121.320218","24.990036,121.312115","25.000169,121.29907","24.992613,121.309039","24.993173,121.299867","24.991055,121.279739","24.998053,121.314183","25.014418,121.305406","25.000169,121.29907","24.954122,121.222352","24.989649,121.179025","24.971664,121.233323","24.954122,121.222352","24.957205,121.236532","24.970683,121.253303","24.97905,121.245271","24.954058,121.238662","24.962395,121.219752","24.938424,121.234404","24.961529,121.261832","25.013894,121.210535","24.953763,121.223122","25.063402,121.195741","25.063402,121.195741","25.035912,121.083027","25.104005,121.243801","25.044182,121.232071","25.047262,121.142981","25.01266,121.136204","25.075191,121.162448","25.073202,121.257167","25.116666,121.244824","24.906857,121.145736","24.909215,121.160716","24.972535,121.106582","24.906857,121.145736","24.987538,121.044835","24.959062,121.038523","24.949556,121.118665","24.918697,121.183544","24.912606,121.09305","24.935768,121.081899","24.958454,121.165365","24.913036,121.207561","24.913036,121.207561","24.942729,121.206452","24.941941,121.22089","24.909153,121.231181","24.923878,121.247322","24.941941,121.22089","24.991493,121.33759","25.045306,121.31464","24.990025,121.333531","25.053802,121.381652","25.055487,121.357017","25.019329,121.409155","24.995264,121.338647","24.938295,121.308621","24.95596,121.302755","24.929587,121.284821","24.970363,121.324102","24.95011,121.283827","24.975784,121.271929","24.938295,121.308621","25.047294,121.293286","25.060371,121.271501","25.039468,121.241989","25.041349,121.284869","25.083493,121.304172","25.047473,121.293497","24.868657,121.20764","24.864084,121.214598","24.872663,121.234338","24.868657,121.20764","24.842766,121.241306","24.835813,121.206913","24.850511,121.175389","24.868657,121.20764","24.880348,121.287106","24.822988,121.259764","24.904856,121.283791","24.880725,121.265271","24.930275,121.31839","24.89424,121.324083","24.859345,121.281233","24.857849,121.295029","24.849176,121.286503","24.825101,121.309251","24.83795,121.348445","24.814182,121.350961","24.816666,121.38393","24.993628,121.30098","24.799327,121.365585","24.779475,121.325569","24.799741,121.307177","24.719675,121.331288","24.665222,121.359881","24.655189,121.399516","24.68566,121.398333","24.839836,121.292855","24.904856,121.283791"};
        String[] phone = {"03-3334400","03-3323811","03-3622987","03-3364224","03-3026731","03-3322762","03-3602209","03-3602042","03-3332742","03-3568289","03-3411019","03-4221460","03-4982260","03-4523093","03-4222030","03-4563524","03-4553845","03-4528348","03-4562912","03-4258325","03-4285777","03-2854333","03-4531356","03-4269225","03-3866555","03-3862804","03-4732025","03-3835089","03-3811684","03-4831224","03-4981001","03-3862314","03-3936119","03-3837135","03-4782274","03-4643521","03-4772014","03-4782007","03-4861092","03-4768410","03-4779178","03-4826278","03-4727925","03-4721794","03-4205433","03-4390218","03-4391280","03-4938381","03-4282206","03-4502331","03-4503245","03-4686232","03-329-2701","03-3251310","03-3500070","03-3186447","03-3282570","02-82006815","03-3202554","03-3731931","03-3662771","03-3682802","03-3644106","03-3775127","03-3692187","03-3645846","03-2220307","03-3223981","03-3232704","03-3121090","03-3241749","03-2129924","03-4890831","03-4792002","03-4797497","03-4891110","03-4712021","03-4717204","03-4794384","03-4804165","03-3883851","03-3883720","03-3801287","03-3801040","03-3801853","03-387-5802","03-3883849","03-3882349","03-3888767","03-3885884","03-3825605","03-7333472","03-3822607","03-3822958","03-3822606","03-3822960","03-3822960","03-3821016","03-3912258","03-3912253","03-3912180","03-3885891","03-3072240"};
        if(location == null){
            for(int i=0; i<name.length; i++){
                HashMap <String,String> item = new HashMap<String,String>();
                item.put("name", name[i]);
                item.put("address", address[i]);
                item.put("phone", phone[i]);
                if(country.equals(address[i].substring(3,6))){
                    list.add( item );
                }

            }
        }else{
            int shortest_location = -1;
            float shortest = 0;
            for(int i=0; i<name.length; i++){
                String lat_string = lat[i];
                String[] lat_array = lat_string.split(",");

                Location l = new Location("");
                l.setLatitude(Double.parseDouble(lat_array[0]));
                l.setLongitude(Double.parseDouble(lat_array[1]));
                if(shortest == 0)
                    shortest = currentLocation.distanceTo(l);

                if(currentLocation.distanceTo(l) < shortest){
                    Log.v("1", String.valueOf(shortest));
                    Log.v("1", String.valueOf(currentLocation.distanceTo(l)));
                    shortest = currentLocation.distanceTo(l);
                    shortest_location = i;
                }



//                HashMap <String,String> item = new HashMap<String,String>();
//                item.put("name", name[i]);
//                item.put("address", address[i]);
//                item.put("phone", phone[i]);
//                if(country.equals(address[i].substring(3,6))){
//                    list.add( item );
//                }

            }
            HashMap <String,String> item = new HashMap<String,String>();
            item.put("name", name[shortest_location]);
            item.put("address", address[shortest_location]);
            item.put("phone", phone[shortest_location]);
            list.add( item );

            Log.v("shortest", String.valueOf(shortest));



        }


        ArrayAdapter<HashMap> adapter = new ArrayAdapter<HashMap>(this, android.R.layout.simple_list_item_2, android.R.id.text1, list) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                HashMap <String,String> item1 = list.get(position);

                text1.setText(item1.get("name") + item1.get("phone"));
                text2.setText(item1.get("address"));
                return view;
            }
        };

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap <String,String> item1 = list.get(position);
                String phone = item1.get("phone");
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone));
                mContext.startActivity(callIntent);
            }
        });
        // Bind to our new adapter.

        listview.setAdapter(adapter);

    }

}
