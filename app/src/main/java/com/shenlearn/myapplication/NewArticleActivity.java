package com.shenlearn.myapplication;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.HashMap;

import API.pickhug.ArticleService;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;

public class NewArticleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_article);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public void new_article(View view) {
        final ArticleService articleservice = new ArticleService();
        final ProgressDialog ringProgressDialog = ProgressDialog.show(NewArticleActivity.this, "", "請稍候 ...", false, false);

        HashMap params = new HashMap();
        EditText content = (EditText) findViewById(R.id.new_article_content);
        params.put("article[content]", content.getText());

        articleservice.create(NewArticleActivity.this, params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Looper.prepare();
                ringProgressDialog.dismiss();
                articleservice.error_handler(NewArticleActivity.this, 999, "");
                Looper.loop();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                Looper.prepare();
                RegisterActivity.locked = false;
                Integer statusCode = response.code();
                switch (statusCode) {
                    case 201:
                        ringProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                Intent intent = new Intent();
                                setResult(201, intent);
                                finish();
                            }
                        });

                        break;
                    default:
                        articleservice.error_handler(NewArticleActivity.this, statusCode, response.body().string());
                }
                ringProgressDialog.dismiss();
                Looper.loop();
            }
        });
    }

}
