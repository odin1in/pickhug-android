package API.pickhug;

import android.content.Context;

import com.squareup.okhttp.Callback;

import java.util.HashMap;

import Model.Preferences;

/**
 * Created by odin1in on 15/10/14.
 */
public class ReportService extends APIService{

    public void create(Context _context, HashMap params, HashMap files, Callback callback){
        params.put("accesstoken", new Preferences().getAccessToken(_context));
        super.multi_post("/reports", params, files, callback);
    }
}
