package API.pickhug;

import android.content.Context;

import com.squareup.okhttp.Callback;

import java.util.HashMap;

import Model.Preferences;

/**
 * Created by odin1in on 15/11/5.
 */
public class CommentService extends APIService {

    public void create(Context _context, Integer article_id, HashMap params, Callback callback){
        params.put("accesstoken", new Preferences().getAccessToken(_context));
        super.post("/articles/" + article_id.toString() + "/comments", params, callback);
    }
}
