package API.pickhug;

import android.content.Context;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.RequestBody;

import java.util.HashMap;

import Model.Preferences;

/**
 * Created by odin1in on 15/10/14.
 */
public class UserService extends APIService{

    public void create(HashMap params, Callback callback){
        super.post("/users", params, callback);
    }

    public void forgot(HashMap params, Callback callback){
        super.post("/users/forgot", params, callback);
    }

    public void show(Context _context, Callback callback){
        HashMap params = new HashMap();
        params.put("accesstoken", new Preferences().getAccessToken(_context));

        super.get("/users/show", params, callback);
    }

    public void update(Context _context, HashMap params, Callback callback){
        params.put("accesstoken", new Preferences().getAccessToken(_context));
        params.put("_method", "patch");

        super.post("/users", params, callback);
    }

    public void articles(Context _context, Callback callback){
        HashMap params = new HashMap();
        params.put("accesstoken", new Preferences().getAccessToken(_context));

        super.get("/users/articles", params, callback);
    }

    public void reports(Context _context, Callback callback){
        HashMap params = new HashMap();
        params.put("accesstoken", new Preferences().getAccessToken(_context));

        super.get("/users/reports", params, callback);
    }
}
