package API.pickhug;

import android.content.Context;

import com.shenlearn.myapplication.SessionHelper;
import com.squareup.okhttp.Callback;

import java.util.HashMap;

import Model.Preferences;

/**
 * Created by odin1in on 15/10/14.
 */
public class ArticleService extends APIService{

//    public void create(HashMap params, Callback callback){
//        super.post("/users", params, callback);
//    }

    public void index(Context _context, Callback callback){
        HashMap params = new HashMap();
        params.put("accesstoken", new Preferences().getAccessToken(_context));

        super.get("/articles", params, callback);
    }

    public void create(Context _context, HashMap params, Callback callback){
        params.put("accesstoken", new Preferences().getAccessToken(_context));
        super.post("/articles", params, callback);
    }

    public void know(Context _context, Integer article_id, Callback callback){
        HashMap params = new HashMap();
        params.put("accesstoken", new Preferences().getAccessToken(_context));
        super.post("/articles/" + article_id.toString() + "/know", params, callback);
    }
}
