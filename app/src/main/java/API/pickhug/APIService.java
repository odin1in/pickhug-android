package API.pickhug;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.shenlearn.myapplication.SessionHelper;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

public class APIService {
    static String api_url = "http://10.0.3.2:3000/api/v1";
//    static String api_url = "http://pickhug.herokuapp.com/api/v1";

    public void get(String url, HashMap params, Callback callback){
        HttpUrl a = HttpUrl.parse(api_url);
        HttpUrl.Builder builder = new HttpUrl.Builder().scheme(a.scheme()).host(a.host()).port(a.port());
        for (Object key : params.keySet()) {
            builder.addQueryParameter(key.toString(), params.get(key).toString());
        }
        Log.v("1", builder.build().encodedQuery());
        Request request = new Request.Builder()
                .url(api_url + "/" + url + "?" + builder.build().encodedQuery()).build();
        new OkHttpClient().newCall(request).enqueue(callback);
    }

    public void post(String url, HashMap params, Callback callback){
        FormEncodingBuilder builder = new FormEncodingBuilder();
        for (Object key : params.keySet()) {
            builder.add(key.toString(), params.get(key).toString());
        }
        Request request = new Request.Builder()
                                    .url(api_url + "/" + url).post(builder.build()).build();
        new OkHttpClient().newCall(request).enqueue(callback);
    }

    public void multi_post(String url, HashMap params, HashMap files, Callback callback){
        MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
        MultipartBuilder builder = new MultipartBuilder().type(MultipartBuilder.FORM);

        for (Object key : params.keySet()){
            builder.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + key.toString() + "\""),
                    RequestBody.create(null, params.get(key).toString()));
        }

        for (Object key : files.keySet()){
            builder.addFormDataPart(key.toString(), "test.png", RequestBody.create(MEDIA_TYPE_PNG, new File(files.get(key).toString())));
        }

        Request request = new Request.Builder()
                .url(api_url + "/" + url).post(builder.build()).build();
        new OkHttpClient().newCall(request).enqueue(callback);
    }

    public void error_handler(Context context, Integer statusCode, String string){
        Log.v("1",statusCode.toString());

        switch (statusCode) {
            case 403:
                new SessionHelper(context).redirect_to_login();
                break;
            case 404:
                Toast.makeText(context, "找不到該筆資料。", Toast.LENGTH_SHORT).show();
                break;
            case 422:
                Toast.makeText(context, parse_error(string), Toast.LENGTH_SHORT).show();
                break;
            case 998:
                Toast.makeText(context, "無法解析JSON。", Toast.LENGTH_SHORT).show();
                break;
            case 999:
                Toast.makeText(context, "無法連接伺服器。", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, "無法更新，請聯絡管理員。", Toast.LENGTH_SHORT).show();
        }
    }

    public String parse_error(String string){
        String errors = "";
        try{
            JSONObject obj = new JSONObject(string);
            for(int i = 0; i < obj.names().length(); i++){
                if(i > 0) errors += "\n";
                errors += ( obj.names().getString(i) + ": " );
                JSONArray transitListArray = obj.getJSONArray(obj.names().getString(i));
                errors += (  transitListArray.join(",") );
            }

            return errors;
        }catch (Exception error){
            error.printStackTrace();
            errors = "無法解析JSON。";
        }
        return errors;
    }
}
