package Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.shenlearn.myapplication.R;
import com.shenlearn.myapplication.ReplyActivity;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import API.pickhug.ArticleService;
import Model.Article;
import Model.Report;

/**
 * Created by odin1in on 15/10/25.
 */
public class MyReportCardViewDataAdapter extends RecyclerView.Adapter<MyReportCardViewDataAdapter.ViewHolder> {
    public List<Report> mDataset;
    public Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyReportCardViewDataAdapter(Context myContext, List<Report> myDataset) {
        mContext = myContext;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyReportCardViewDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.content_my_report_card, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.datetime_status.setText(mDataset.get(position).getViolateAt() + " " + mDataset.get(position).getStatus());
        viewHolder.note_text.setText(mDataset.get(position).getNote());
        viewHolder.location_text.setText(mDataset.get(position).getLocation());

        Picasso.with(mContext).cancelRequest(viewHolder.pic1);
        if(!mDataset.get(position).getPic1().isEmpty()) {
            Picasso.with(mContext)
                    .load(mDataset.get(position).getPic1())
                    .into(viewHolder.pic1);
        }

        Picasso.with(mContext).cancelRequest(viewHolder.pic2);
        if(!mDataset.get(position).getPic2().isEmpty()) {
            Picasso.with(mContext)
                    .load(mDataset.get(position).getPic2())
                    .into(viewHolder.pic2);
        }

        Picasso.with(mContext).cancelRequest(viewHolder.pic3);
        if(!mDataset.get(position).getPic3().isEmpty()) {
            Picasso.with(mContext)
                    .load(mDataset.get(position).getPic3())
                    .into(viewHolder.pic3);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView datetime_status;
        public TextView note_text;
        public TextView location_text;
        public ImageView pic1;
        public ImageView pic2;
        public ImageView pic3;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            datetime_status = (TextView) itemLayoutView.findViewById(R.id.datetime_status);
            note_text = (TextView) itemLayoutView.findViewById(R.id.note);
            location_text = (TextView) itemLayoutView.findViewById(R.id.location);
            pic1 = (ImageView) itemLayoutView.findViewById(R.id.imageView3);
            pic2 = (ImageView) itemLayoutView.findViewById(R.id.imageView4);
            pic3 = (ImageView) itemLayoutView.findViewById(R.id.imageView5);
        }
    }

}
