package Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.shenlearn.myapplication.ForumActivity;
import com.shenlearn.myapplication.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import API.pickhug.ArticleService;
import Model.Article;

import com.shenlearn.myapplication.ReplyActivity;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;

/**
 * Created by odin1in on 15/10/25.
 */
public class ForumCardViewDataAdapter extends RecyclerView.Adapter<ForumCardViewDataAdapter.ViewHolder> {
    public List<Article> mDataset;
    public Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ForumCardViewDataAdapter(Context myContext, List<Article> myDataset) {
        mContext = myContext;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ForumCardViewDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.forum_card_view, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        viewHolder.content_text.setText(mDataset.get(position).getContent());
        viewHolder.known_text.setText(mDataset.get(position).getKnowsCount().toString() + " 個人懂你");
        viewHolder.datetime_text.setText(mDataset.get(position).getCreatedAt());
        viewHolder.reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ReplyActivity.class);
                intent.putExtra("article_id", mDataset.get(position).getId());
                intent.putExtra("article_raw_data", mDataset.get(position).getRawData());
                mContext.startActivity(intent);
            }
        });

        viewHolder.know.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(final View v) {

                final ArticleService articleservice = new ArticleService();
                final Article article = mDataset.get(position);

                articleservice.know(mContext, mDataset.get(position).getId(), new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        Looper.prepare();
                        articleservice.error_handler(mContext, 999, "");
                        Looper.loop();
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        Looper.prepare();
                        Integer statusCode = response.code();
                        switch (statusCode) {
                            case 201:
                                try {
                                    JSONObject obj = new JSONObject(response.body().string());
                                    article.setKnowsCount(obj.getInt("knows_count"));
                                    v.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            notifyDataSetChanged();
                                        }
                                    });

                                } catch (Exception error) {
                                    articleservice.error_handler(mContext, 999, "");
                                    error.printStackTrace();
                                }

                                break;
                            default:
                                articleservice.error_handler(mContext, statusCode, "");
                        }
                        Looper.loop();
                    }
                });
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView content_text;
        public TextView datetime_text;
        public TextView known_text;
        public Button know;
        public Button reply;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            content_text = (TextView) itemLayoutView.findViewById(R.id.forum_card_view_content);
            datetime_text = (TextView) itemLayoutView.findViewById(R.id.forum_card_view_datetime);
            known_text = (TextView) itemLayoutView.findViewById(R.id.forum_card_view_known);
            know = (Button) itemLayoutView.findViewById(R.id.forum_card_view_know);
            reply = (Button) itemLayoutView.findViewById(R.id.forum_card_view_reply);

        }
    }

}
