package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Report {

    private Integer id;
    private String note;
    private String status;
    private String violate_at;
    private String location;
    private String pic1;
    private String pic2;
    private String pic3;
    private User user;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The note
     */
    public String getNote() {
        return note;
    }

    /**
     *
     * @param content
     * The note
     */
    public void setNote(String content) {
        this.note = content;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param createdAt
     * The status
     */
    public void setStatus(String createdAt) {
        this.status = createdAt;
    }

    /**
     *
     * @return
     * The violate_at
     */
    public String getViolateAt() {
        return violate_at;
    }

    /**
     *
     * @param updatedAt
     * The violate_at
     */
    public void setViolateAt(String updatedAt) {
        this.violate_at = updatedAt;
    }

    public void setLocation(String data){
        this.location = data;
    }

    public String getLocation(){
        return this.location;
    }

    public void setPic1(String pic1){
        this.pic1 = pic1;
    }

    public String getPic1(){
        return this.pic1;
    }

    public void setPic2(String pic1){
        this.pic2 = pic1;
    }

    public String getPic2(){
        return this.pic2;
    }

    public void setPic3(String pic1){
        this.pic3 = pic1;
    }

    public String getPic3(){
        return this.pic3;
    }


}