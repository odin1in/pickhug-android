package Model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by odin1in on 15/10/15.
 */
public class Preferences {
    final String ACCESSTOKEN_KEY = "accesstoken";
    final String USERID_KEY = "user_id";
    final String LOCATION = "com.shenlearn.myapplication";

    public String getAccessToken(Context context){

        SharedPreferences prefs = context.getSharedPreferences(LOCATION, Context.MODE_PRIVATE);
        return prefs.getString(ACCESSTOKEN_KEY, "");
    }

    public String getUserId(Context context){
        SharedPreferences prefs = context.getSharedPreferences(LOCATION, Context.MODE_PRIVATE);
        return prefs.getString(USERID_KEY, "");
    }

    public void setAccessToken(Context context, String accesstoken){

        SharedPreferences prefs = context.getSharedPreferences(LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString(ACCESSTOKEN_KEY, accesstoken).apply();
    }

    public void setUserId(Context context, String user_id){
        SharedPreferences prefs = context.getSharedPreferences(LOCATION, Context.MODE_PRIVATE);
        prefs.edit().putString(USERID_KEY, user_id).apply();
    }

    public void clear(Context context){
        SharedPreferences prefs = context.getSharedPreferences(LOCATION, Context.MODE_PRIVATE);
        prefs.edit().clear().commit();
    }
}
